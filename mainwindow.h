#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <vector>
#include <QMainWindow>
#include <string>
#include <QLabel>
#include <QCheckBox>

using namespace std;

///
/// This class contains all the fucntions necessary to run
/// the PasswordStrength lab. 
///

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    vector<char> V;

    void readPass(const string &pass);
    void strengthDisplay(string strength, int totalScore);


    void setNumberOfCharacters(int count, int score) ;
    void setUpperCharacters(int count, int score) ;
    void setLowerCharacters(int count, int score) ;
    void setDigits(int count, int score) ;
    void setSymbols(int count, int score) ;
    void setMiddleDigitsOrSymbols(int count, int score) ;
    void setRequirements(int count, int score) ;

    void setLettersOnly(int count, int score) ;
    void setDigitsOnly(int count, int score) ;
    void setConsecutiveUpper(int count, int score) ;
    void setConsecutiveLower(int count, int score) ;
    void setConsecutiveDigits(int count, int score) ;



private slots:
    void on_lineEdit_textChanged(const QString &arg1);

    void on_HiddenCheckBox_clicked(bool checked);


private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
