/*
RAN 2014-08-20
-- Simplified the upperCheck, lowerCheck, etc.. repeatChars, functions
-- Fixed many of the warnings
-- Eliminated findLetterpos, alpha[]

RAN 2014-08-21
-- Las funciones de contar consecutivos tienen error.  Estoy arreglando.
-- Cree una nueva función "generica" para contar consectuivos: countRepeated
*/


#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "psfunctions.h"
#include <vector>
#include <QDebug>
#include <string>
using namespace std;



///
/// Main function, where all the other PasswordStrength 
/// functions are executed
///
void MainWindow::readPass(const string &pass){
    string strength ="Compute Me!" ;
    int totalScore = 0;
    int requirements = 0;
    int digit_count = 0 ;
    int symbol_count = 0 ;
    int count ;
    int score ;
    int len ;
    
    /*
     * Password Additions:
     */
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    // Check the length of the string (min:8, better>8, n*4)
       len = count = pass.length() ;
       score = 4 * count ;
       setNumberOfCharacters(count, score);
       totalScore += score ;

    // Uppercase letters (good-min:1, better-min:2, (len-n)*2)
       count = countUppercase(pass) ;
       score=0;
       if(count) {
           requirements += 1;
           score = (len - count) * 2 ;
        }
       setUpperCharacters(count, score);
       totalScore += score ;

   
    
    // ADD YOUR CODE HERE
    
    
    
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    //Controls progress bar, compute strength
    
    // ADD YOUR CODE HERE

        strengthDisplay(strength, totalScore);
}

