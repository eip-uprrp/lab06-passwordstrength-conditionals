#Lab. 6: Estructuras de decisión - "Password Strength"

![](http://demo05.cloudimage.io/s/resize/215/i.imgur.com/1srD4sm.png?1)
![](http://demo05.cloudimage.io/s/resize/215//i.imgur.com/9kGfqsV.png?1)
![](http://demo05.cloudimage.io/s/resize/215/i.imgur.com/9U2BE9X.png)



En casi todas las instancias en que queremos resolver un problema hay una o más opciones que dependen  de si se cumplen o no ciertas condiciones. Los programas de computadoras se construyen para resolver problemas y, por lo tanto, deben tener una estructura que permita tomar decisiones. En C++ las  instrucciones de decisión (o condicionales) se estructuran utilizando `if`, `else`, `else if` o `switch`. En la experiencia de laboratorio de hoy practicarás el uso de algunas de estas estructuras completando el diseño de una aplicación que determina cuan "fuerte" o resistente es una contraseña de acceso ("password"). 

##Objetivos:

1.  Utilizar expresiones relacionales y seleccionar operadores lógicos adecuados para la toma de decisiones.
2. Aplicar estructuras de decisión.


##Pre-Lab:

Antes de llegar al laboratorio debes:

1. haber repasado los conceptos relacionados a estructuras de decisión.

2. haber repasado el uso de objetos de la clase `string` y su método `length()`.

3. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

4. haber tomado el [quiz Pre-Lab 6](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=7133) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).

##Fortaleza de contraseñas de acceso

El utilizar contraseñas de acceso resistentes es escencial para mantener los datos seguros en los sistemas de información. Una contraseña se considera resistente o fuerte ("strong") si resulta costo-inefectivo para el "pirata informático" ("hacker") emplear tiempo tratando de adivinarla usando contraseñas ingenuas o fuerza bruta. Por ejemplo, una contraseña que consiste en una palabra simple del diccionario, sin números, símbolos o letras mayúsculas, es tan fácil de descifrar que hasta "un cavernícola puede hacerlo".

Como no existe un sistema oficial para medir las contraseñas, utilizaremos fórmulas creadas por el "passwordmeter"  para evaluar la fortaleza general de una contraseña dada [1]. La fortaleza de la contraseña se cuantificará otorgando puntos por utilizar "buenas" técnicas de selección de contraseñas (como utilizar símbolos y letras) y restando puntos por utilizar "malos" hábitos en las contraseñas (como utilizar solo letras minúsculas o símbolos consecutivos de un mismo tipo).

Las siguientes tablas resumen los valores añadidos y sustraidos para varias características en las contraseñas.



### Asignando puntuación a las contraseñas

#### Sumas

|      | Categoría                        | Puntos                                   | Notas                                                |
| :--- | :-------------------------------- | :-------------------------------------: | ----------------------------------------------------: |
| 1.   | Número de caracteres             | `4(len)`                                   | *len* es el largo de la contrasena                        |
| 2.   | Letras mayúsculas                | ![](http://i.imgur.com/Pp21yYJ.gif)     | *n* es el número de letras mayúsculas                |
| 3.   | Letras minúsculas                | ![](http://i.imgur.com/0GLIVSG.gif)     | *n* es el número de letras minúsculas                |
| 4.   | Dígitos                          | ![](http://i.imgur.com/Q6bkVob.gif)     | *n* es el número de dígitos                          |
| 5.   | Símbolos                         | ![](http://i.imgur.com/KaZrZQI.gif)     | *n* es el número de símbolos                         |
| 6.   | Dígitos o símbolos en el medio   | `2n`                                    | *n* es el número de dígitos y símbolos en el medio   |
| 7.   | Requisitos                       | `2n`                                    | *n* es el número de requisitos que se cumplen        |



Lo que sigue son algunos detalles adicionales y ejemplos de los criterios para **sumas**.

1. **Número de caracteres**: este es el criterio más directo. La puntuación es 4 veces el largo de la contraseña. Por ejemplo, `"ab453"` tendría puntuación de `4(5)= 20`.

2. **Letras mayúsculas** La puntuación es `2 (len - n)` si la contraseña consiste de una mezcla de letras mayúsculas **Y** al menos otro tipo de caracter (minúscula, dígitos, símbolos). De lo contrario, la puntuación es `0`. Por ejemplo,

    a. la puntuación para `"ab453"` sería `0` ya que no tiene letras mayúsculas.

    b. la puntuación para `"ALGO"` sería `0` porque **solo** contiene letras mayúsculas.

    c. la puntuación para `"SANC8in"` sería `2 (7-4) = 6` porque la contraseña es de largo `7`, contiene `4` letras mayúsculas, y contiene caracteres de otro tipo.

3. **Letras minúsculas** La puntuación es `2 (len - n)` si la contraseña consiste de una mezcla de letras minúsculas **Y** al menos otro tipo de caracter (mayúscula, dígitos, símbolos). De lo contrario, la puntuación es `0`. Por ejemplo,

    a. la puntuación para `"ab453"` sería `2 (5-2) = 6` porque la contraseña es de largo `5`, contiene `2` letras minúsculas, y contiene caracteres de otro tipo.

    b. la puntuación para `"ALGO"` sería `0` porque no contiene letras minúsculas.

    c. la puntuación para `"sancochin"`  sería `0` porque contiene **solo** letras minúsculas.

4. **Dígitos** La puntuación es `4n` si la contraseña consiste de una mezcla de dígitos **Y** al menos otro tipo de caracater (minúscula, mayúscula, símbolos). De otro modo la puntuación es `0`. Por ejemplo,

  a. la puntuación para `"ab453"` sería `4 (3) = 12` porque la contraseña contiene `3` dígitos y contiene caracteres de otro tipo.

  b. la puntuación para `"ALGO"` sería $0$ porque no tiene dígitos.

  c. la puntuación para `801145555`  sería `0` porque contiene **solo** dígitos.

5. **Símbolos** La puntuación es `6n` si la contraseña consiste de una mezcla de símbolos. De otro modo la puntuación es `0`. Por ejemplo,

  a. la puntuación para `"ab453"` sería `0` porque no contiene símbolos.

  b. la puntuación para `"ALGO!!"` sería `6 (2)` porque contiene `2` símbolos y contiene otros tipos de caracteres.

  c. la puntuación para `---><&&`  sería `6(7) = 42` porque contiene `7` símbolos. Nota que en el caso de símbolos, se otorga puntuación incluso cuando no hay otro tipo de caracteres.

6. **Dígitos o símbolos en el medio** La puntuación es `2n` si la contraseña contiene símbolos o dígitos que no están en la primera o última posición. Por ejemplo,

  a. la puntuación para `"ab453"` sería `2 (2) = 4` porque contiene dos dígitos que no están en la primera o última posición, estos son `4` y `5`.

  b. la puntuación para `"ALGO!"` sería `0` porque no contiene dígitos ni símbolos en el medio, el único símbolo está al final.

  c. la puntuación para `S&c8i7o!`  sería `2 (3) = 6` porque contiene `3` símbolos o dígitos en el medio, estos son `&`, `8`, y `7`.

7. **Requisitos**: La puntuación es `2n`, donde `n` es el número de *requisitos* que se cumplen. Los requisitos son:

    a. La contraseña debe tener 8 o más caracteres de largo.

    b. Debe contener *al menos 3* de los siguientes renglones
        - Letras mayúsculas
        - Letras minúsculas
        - Números
        - Símbolos

    Se otorga  `2n` solo si el criterio del largo **Y** 3 o 4 de los otros criterios se cumplen. Por ejemplo,

      a. la puntuación para `"ab453"` sería `0` porque el criterio del largo no se cumple.

      b. la puntuación para `"abABCDEF"` sería `0` debido a que, a pesar de que se cumple el criterio del largo, solo 2 de los 4 otros criterios se cumplen (mayúsculas y minúsculas).

      c. la puntuación para `"abAB99!!"` sería `2 (5) = 10` debido a que cumple la condición del largo y también los otros 4 criterios.


#### Restas


|      | Categoría                         | Puntos                                   | Notas                                                                   |
| :--- | :-------------------------------- | :-------------------------------------: | ----------------------------------------------------:                   |
| 1.   | Solo letras                       | ![](http://i.imgur.com/HltS4m3.gif)     | *len* es el largo de la contrasena                                           |
| 2.   | Solo números                      | ![](http://i.imgur.com/rulD7tG.gif)     | *len* es el largo de la contrasena                                           |
| 3.   | Letras mayúsculas consecutivas    | `-2n`                                   | *n* es el número de letras mayúsculas que siguen a otra letra mayúscula |
| 4.   | Letras minúsculas consecutivas    | `-2n`                                   | *n* es el número de letras minúsculas que siguen a otra letra minúscula                                                                     |
| 5.   | Dígitos consecutivos              | `-2n`                                   | *n* es el número de dígitos que siguen a otro dígito                     |


Lo que sigue son algunos detalles adicionales y ejemplos de los criterios para **restas**.

1. **Letras solamente**: La puntuación es `-len` para una contrseña que consista solo de letras, de otro modo obtiene `0`. Por ejemplo, 

    a. la puntuación para `"ab453"` sería `0` ya que contiene solo letras y números

    b. la puntuación para `"Barrunto"` sería `-8` ya que consiste solo de letras y su largo es `8`.

2. **Dígitos solamente**: La puntuación es `-len` para una contraseña que consista solo de dígitos, de otro modo obtiene `0`. Por ejemplo, 

    a. la puntuación para `"ab453"` sería `0` ya que contiene solo letras y números

    b. la puntuación para `987987987` sería `-9` ya que consiste solo de dígitos y su largo es `9`.

3. **Letras mayúsculas consecutivas**: La puntuación es `-2n` donde *n* es el número de letras mayúsculas que siguen a otra letra mayúscula. Por ejemplo,

    a. la puntuación para `"DB453"` sería `-2 (1) = -2` ya que solo contiene una letras mayúscula (`B`) que sigue a otra letra mayúscula.

    b. la puntuación para `"TNS1PBMA"` sería `-2 (5) = -10` ya que contiene 5 letras mayúsculas (`N`, `S`, `B`, `M`, `A`) que siguen a otra letra mayúscula.

4. **Letras minúsculas consecutivas**: Igual que el criterio #3 pero para letras minúsculas.

5. **Dígitos consecutivos**: Igual que el criterio #3 pero para dígitos.



##Sesión de laboratorio:

En esta experiencia de laboratorio practicarás el uso de expresiones matemáticas y estructuras condicionales para computar la puntuación de fortaleza de una contraseña combinando las puntaciones de los criterios individuales.

La experiencia está diseñada para evaluar la resistencia o fortaleza de contraseñas. La reacción visual inmediata le proveerá al usuario una manera de mejorar su contraseña y está enfocada en romper los malos hábitos típicos de formular contraseñas débiles. 

Tu tarea es completar el diseño de la aplicación para medir la fortaleza de las contraseñas de acceso ("password strength"). Al final obtendrás un programa que será una versión simplificada de la aplicación en http://www.passwordmeter.com/. Como no existe un sistema oficial para medir las contraseñas, se utilizarán las fórmulas creadas por el "passwordmeter" para evaluar la fortaleza general de una contraseña dada. El programa permitirá al usuario entrar una contraseña y calculará su fortaleza utilizando una serie de reglas.

La fortaleza de la contraseña se cuantificará otorgando puntos por utilizar "buenas" técnicas de selección de contraseñas (como utilizar símbolos y letras) y restando puntos por utilizar "malos" hábitos en las contraseñas (como utilizar solo letras minúsculas o símbolos consecutivos de un mismo tipo). Tu programa analizará la contraseña dada por el usuario y usará los criterios en las tablas presentadas arriba para computar una puntuación para la fortaleza de la contraseña.



###Instrucciones



1.	Abre un terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab06-passwordstrength-conditionals.git` para descargar la carpeta `Lab06-PasswordStrength-Conditionals` a tu computadora.

2.	Haz doble "click" en el archivo `passwordStrength.pro` para cargar este proyecto a Qt. 

3. El proyecto consiste de varios archivos:

      * `psfunctions.cpp` : contiene las implantaciones de las funciones que tu programa va a invocar para calcular la puntuación de la fortaleza total de la contrasena. **No tienes que cambiar nada del código en este archivo ni en el archivo `psfunctions.h`**. Simplemente invocarás desde la función `readPass` en el archivo `readpassword.cpp` las funciones contenidas en ellos, según sea necesario. Hay funciones que no necesitarás invocar.
      * `psfunctions.h` : contiene los prototipos de las funciones definidas en `psfunctions.cpp`.
     
    **Los cambios se van a hacer en el archivo `readpassword.cpp`, en los demás archivos no debes que cambiar nada.**

    ### Funciones para actualizar la interface gráfica de usuarios.

  En el ejercicio de laboratorio vas a necesitar invocar funciones para actualizar la interface gráfica de usuarios.  Cada uno de los criterios para evaluar la fortaleza de la contraseña debe tener una función que reciba  la  cantidad de caracteres que cumple con el criterio y la puntuación calculada.

  El encabezado de cada una de las funciones debe ser similar a:

  ```
  void setCRITERIO(int count, int score) ;
  ```

  donde CRITERIO debe reemplazarse por el criterio evaluado.

  Las funciones son:
  ```
    // Para actualizar el numero total de caracteres en la contraseña.
    void setNumberOfCharacters(int count, int score) ;  

    // Para las sumas

    // Para actualizar los caracteres en mayuscula.
    void setUpperCharacters(int count, int score) ;

    // Para actualizar los caracteres en minuscula.
    void setLowerCharacters(int count, int score) ;

    // Para actualizar los caracteres que son digitos.
    void setDigits(int count, int score) ;

    // Para actualizar los caracteres que son símbolos.
    void setSymbols(int count, int score) ;

    // Para actualizar digitos o simbolos en el medio
    void setMiddleDigitsOrSymbols(int count, int score) ;

    // Para actualizar el criterio de  los requisitos
    void setRequirements(int count, int score) ;

    // Para las restas

    // Para actualizar el criterio de letras solamente.
    void setLettersOnly(int count, int score) ;

    // Para actualizar el criterio de dígitos solamente.
    void setDigitsOnly(int count, int score) ;

    // Para actualizar el criterio de mayusculas consecutivas.
    void setConsecutiveUpper(int count, int score) ;

    // Para actualizar el criterio de minusculas consecutivas.
    void setConsecutiveLower(int count, int score) ;

    // Para actualizar el cretierio de digitos consecutivos.
    void setConsecutiveDigits(int count, int score) ; 
  ```


4. El código que te proveemos contiene las funciones que computan la puntuación dada por la mayoría de los criterios. [Aquí](http://ccom.uprrp.edu/~rarce/ccom3033f14/documentation/passwordStrengthCL/psfunctions_8cpp.html) hay una lista y descripción de las funciones. Tu tarea es utilizar expresiones matemáticas y estructuras condicionales para computar la puntuación total de fortaleza de una contraseña combinando las puntaciones de los criterios individuales.

    En la parte superior de la interface gráfica se ingresa la contraseña.  Debajo aparece un *informe* que contiene los distintos criterios, el conteo para cada criterio, y la puntuación individual para los criterios. La puntuación total será la suma de todas los puntos (sumas y restas) de los criterios individuales.

    Basado en la puntuación total, el programa debe clasificar la fortaleza de la contraseña como:

| Puntación total |  Fortaleza  |
|-----------------|-------------|
| [0,20)          | Bien débil  |
| [20,40)         | Débil       |
| [40,60)         | Buena       |
| [60,80)         | Fuerte      |
| [80,100)        | Bien fuerte |

e invocar la función `strengthDisplay` con la fortaleza calculada y la puntuación final.



**Ejemplo 1:**

Contraseña: `caba77o`

![](http://demo05.cloudimage.io/s/resize/400/i.imgur.com/tcCYgZC.png)


**Ejemplo 2:**

Contraseña: `S1nf@nia!`

![](http://demo05.cloudimage.io/s/resize/400/i.imgur.com/9U2BE9X.png)


En el Ejemplo 2, el conteo de los **requisitos** es 5 porque `"S1nf@nia!"` cumple con el criterio de largo y también contiene mayúsculas, minúsculas, números y símbolos. Por lo tanto, la puntuación del número de requisitos es `2(5)=10`.

En el código del proyecto vas a encontrar un ejemplo inicial que incluye como calcular los primeros dos criterios positivos, el número de caracteres en la contraseña y el número de letras mayúsculas. Puedes compilar y ejecutar el ejemplo para que veas la interface funcionando con esos dos criterios.  Tu tarea es añadir el código para computar las puntuaciones de los demás criterios.

###Entrega

Copia el código que creaste (incluyendo las declaraciones de las variables) para determinar la fortaleza de la contraseña en un archivo y [súbelo a Moodle utilizando este enlace](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=7138).

**NOTA** Para que el código se vea bien en la caja en donde pegas tu código, selecciona la opción `Preformatted` en el menú en donde dice `Paragraph`.

##Referencias

[1] Passwordmeter, http://www.passwordmeter.com/