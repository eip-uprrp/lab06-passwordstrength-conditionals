#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QLineEdit>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    // The function the student needs to work with
    readPass(arg1.toStdString());
}

void MainWindow::on_HiddenCheckBox_clicked(bool checked)
{
    if (checked) ui->lineEdit->setEchoMode(QLineEdit::Password);
    else ui->lineEdit->setEchoMode(QLineEdit::Normal);
}

void MainWindow::setNumberOfCharacters(int count, int score){
    ui->NumberOfCharCount->setText(QString::number(count));
    ui->NumberOfCharScore->setText(QString::number(score));
    ui->NumberCharCheckBox->setChecked(count > 0);


}

void MainWindow::setUpperCharacters(int count, int score){
    ui->UppercaseCountLabel->setText(QString::number(count));
    ui->UppercaseScoreLabel->setText(QString::number(score));
    ui->UppercaseCheckBox->setChecked(count > 0);
}

void MainWindow::setLowerCharacters(int count, int score){
    ui->LowerCaseCountLabel->setText(QString::number(count));
    ui->LowerCaseScoreLabel->setText(QString::number(score));
    ui->LowercaseCheckBox->setChecked(count > 0);
}

void MainWindow::setDigits(int count, int score){
    ui->NumbersCountlabel->setText(QString::number(count));
    ui->NumberScorelabel->setText(QString::number(score));
    ui->NumbersCheckBox->setChecked(count > 0);
}

void MainWindow::setSymbols(int count, int score){
    ui->SymbolsCountlabel->setText(QString::number(count));
    ui->SymbolScorelabel->setText(QString::number(score));
    ui->SymbolsCheckBox->setChecked(count > 0);

}

void MainWindow::setMiddleDigitsOrSymbols(int count, int score){
    ui->MidNumOrSymCountlabel->setText(QString::number(count));
    ui->MidNumOrSymScorelabel->setText(QString::number(score));
    ui->MiddleCheckBox->setChecked(count > 0);
}

void MainWindow::setRequirements(int count, int score){
    ui->ReqCountlabel->setText(QString::number(count));
    ui->ReqScorelabel->setText(QString::number(score));
    ui->RequirementCheckBox->setChecked(count > 0);
}

void MainWindow::setLettersOnly(int count, int score){
    ui->LettersOnlyCountLabel->setText(QString::number(count));
    ui->LettersOnlyScoreLabel->setText(QString::number(score));
    ui->LettersOnlyCheckBox->setChecked(count > 0);
}

void MainWindow::setDigitsOnly(int count, int score){
    ui->NumOnlyCountLabel->setText(QString::number(count));
    ui->NumOnlyScoreLabel->setText(QString::number(score));
    ui->NumbersOnlyCheckBox->setChecked(count > 0);
}

void MainWindow::setConsecutiveUpper(int count, int score){
    ui->ConsUpperCountLabel->setText(QString::number(count));
    ui->ConsUpperScoreLabel->setText(QString::number(score));
    ui->ConsecutiveUpperCheckBox->setChecked(count > 0);
}

void MainWindow::setConsecutiveLower(int count, int score){
    ui->ConsLowerCountLabel->setText(QString::number(count));
    ui->ConsLowerScoreLabel->setText(QString::number(score));
    ui->ConsecutiveLowerCheckBox->setChecked(count > 0);
}

void MainWindow::setConsecutiveDigits(int count, int score){
    ui->ConsNumsCountLabel->setText(QString::number(count));
    ui->ConsNumsScoreLabel->setText(QString::number(score));
    ui->ConsecutiveNumbersCheckBox->setChecked(count > 0);
}

///
/// Function which displays the strength of the
/// password tested by the user
///
void MainWindow::strengthDisplay(string strength, int totalScore){

    if(totalScore > 100){
        totalScore = 100;
        QString percent = QString::number(totalScore) + "%";
        ui->progressBar->setValue(totalScore);
        ui->progressLabel->setText(percent);
    }
    else if(totalScore < 0){
        totalScore = 0;
        QString percent = QString::number(totalScore) + "%";
        ui->progressBar->setValue(totalScore);
        ui->progressLabel->setText(percent);
    }
    else{
        QString percent = QString::number(totalScore) + "%";
        ui->progressBar->setValue(totalScore);
        ui->progressLabel->setText(percent);
    }


    ui->statusLabel->setText(QString::fromStdString(strength));
}




